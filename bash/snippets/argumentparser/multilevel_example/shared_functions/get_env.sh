if [[ -z ${SCRIPT_BASE_DIR} ]]
then
    echo 'Variable SCRIPT_BASE_DIR needs to be set!'
    exit 1
fi

MODULE_DIR=${SCRIPT_BASE_DIR}/modules # Folder containing the modules
SHARED_FUNCTIONS_DIR=${SCRIPT_BASE_DIR}/shared_functions # Folder containing the modules

# Load all shared functions
for file_name in $(find ${SHARED_FUNCTIONS_DIR} -not -path '*get_env.sh' -path '*.sh' -type f)
do
    source ${file_name}
done
