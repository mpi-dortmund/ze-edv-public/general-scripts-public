#############################
############## Dynamic option
#############################

# !!!!!!!!!!!
# Decalare a dictionary/hashtable with the commandline options
# !!!!!!!!!!!

# This needs to be used in the dictionary
OPTION_SEP="1238sdf4134"
function dynamic_parser(){

    function do_unit_test() {
        echo "No unit tests installed, yet"
    }


    for key in "${!original_vars[@]}"
    do
        # Remove leading and trailing whitespaces from the help
        cleaned_version=$(echo "${original_vars[${key}]}" | sed 's|^ *||g' | sed 's| *$||g' | sed "s| *${OPTION_SEP} *|${OPTION_SEP}|g")
        original_vars[${key}]="${cleaned_version}"
        vars[${key}]="${original_vars[${key}]}"
    done


    # This is dynamic to create the help based on the dictionary
    usage="
    Usage:
    bash ${0} "

    regex_split="^\(.*\)${OPTION_SEP}\(.*\)${OPTION_SEP}\(.*\)$"

    unset_array=()
    has_arguments=false
    tmp_usage=""
    for key in "${!vars[@]}"
    do
        default_value=$(echo "${vars[${key}]}" | sed "s|${regex_split}|\1|g")
        sanity_check=$(echo "${original_vars[${key}]}" | sed "s|${regex_split}|\3|g")
        if ! [[ ${key} =~ _SUPPRESS$ ]]
        then
            if [[ ${sanity_check} = BOOL ]]
            then
                tmp_usage+="--${key,,} "
            elif [[ ${key} = ARGUMENTS ]]
            then
                usage+="arg1 arg2 .. argN "
                has_arguments=true
            else
                tmp_usage+="--${key,,}=$(echo "${vars[${key}]}" | sed "s|${regex_split}|\1|g") "
            fi
        else
            use_key=${key%_SUPPRESS}
            tmp_usage+="--${use_key,,} "
            vars[${use_key}]="${vars[${key}]}"
            unset vars[${key}]
            unset_array=("${unset_array[@]}" "${use_key}")
        fi
    done
    usage+="${tmp_usage}
    "

    if [[ ${has_arguments} = true ]]
    then
        default_value=$(echo "${vars["ARGUMENTS"]}" | sed "s|${regex_split}|\1|g")
        usage+="
    ARGUMENTS
        Description: $(echo "${vars["ARGUMENTS"]}" | sed "s|${regex_split}|\2|g")
        Type: $(echo "${vars["ARGUMENTS"]}" | sed "s|${regex_split}|\3|g")
        Default: ${default_value}

    "
        unset vars["ARGUMENTS"]
    fi

    for key in "${!vars[@]}"
    do
        default_value=$(echo "${vars[${key}]}" | sed "s|${regex_split}|\1|g")
        usage+="--${key,,}
        Description: $(echo "${vars[${key}]}" | sed "s|${regex_split}|\2|g")
        Type: $(echo "${vars[${key}]}" | sed "s|${regex_split}|\3|g")
        Default: ${default_value}
    "
        vars[${key}]="${default_value}"

    done
    for entry in ${unset_array[@]}
    do
        unset vars[${entry}]
    done

    # Fill the dictionary with the provided options dynamically
    commandline_args=("${@}")
    for key in "${!vars[@]}"
    do
        sanity_check=$(echo "${original_vars[${key}]}" | sed "s|${regex_split}|\3|g")
        next=false
        new_cmd=()
        for arg in "${commandline_args[@]}"
        do
            if [[ ${next} != false ]]
            then
                missing_argument=false
                for key_comp in "${!vars[@]}"
                do
                    if [[ "${arg}" =~ --${key_comp,,} ]]
                    then
                        missing_argument=true
                    fi
                done

                if [[ ${missing_argument} = true ]]
                then
                    echo "ERROR: option --${key,,} cannot be empty!"
                    exit 1
                fi
                vars["${key}"]="${arg}"
                next=false
            elif [[ ${arg} =~ --${key,,}= ]]
            then
                vars["${key}"]="${arg#*=}"
                next=false
            elif [[ ${arg} =~ ^--${key,,}$ ]]
            then
                next=false
                if [[ ${sanity_check} = BOOL ]]
                then
                    vars["${key}"]=true
                else
                    vars["${key}"]=""
                    next=${key}
                fi
            else
                new_cmd=("${new_cmd[@]}" "${arg}")
            fi
        done
        commandline_args=("${new_cmd[@]}")
    done

    do_setx=false
    for i in "${commandline_args[@]}"
    do
    case $i in

        -h|--help)
        # Use help also with -h even though it is not put in the help automatically, yet.
        echo "${usage}"
        exit 1
        shift # past argument=value
        ;;

        --verbose_setx)
        # Enable set -x after succesful error check
        do_setx=true
        shift
        ;;

        --test_me)
        # Run unit tests
        do_unit_test
        exit 1
        shift # past argument=value
        ;;

        *)
        # Everything else is treated as an input folder
        ARGS=("${ARGS[@]}" "$i")
        shift
        ;;
    esac
    done

    error=false
    for key in "${!vars[@]}"
    do
        sanity_check=$(echo "${original_vars[${key}]}" | sed "s|${regex_split}|\3|g")
        value="${vars[${key}]}"
        if [[ ${sanity_check} = BOOL ]]
        then
            : # pass do nothing
        elif [[ -z ${value} ]]
        then
            echo "--${key,,}" cannot be empty!
            error=true
        elif [[ ${sanity_check} = ANY ]]
        then
            : # pass do nothing
        elif [[ ${sanity_check} = FILE ]]
        then
            if [[ ! -f ${value} ]]
            then
                echo Input to --${key,,} needs to be an existing file!
                error=true
            fi
        elif [[ ${sanity_check} = DIR ]]
        then
            if [[ ! -d ${value} ]]
            then
                echo Input to --${key,,} needs to be an existing directory!
                error=true
            fi
        else
            sanity_regex=${sanity_check}
            sanity_message=${sanity_regex}
            case ${sanity_check} in
                INT)
                sanity_regex=^[0-9-]*$
                ;;
                UINT)
                sanity_regex=^[0-9]*$
                ;;
                FLOAT)
                sanity_regex=^[0-9.-]*$
                ;;
            esac
            if [[ ! ${value} =~ ${sanity_regex} ]]
            then
                echo Input to --${key,,} needs to follow ${sanity_message}
                error=true
            fi
        fi
    done

    if [[ ${error} = true ]]
    then
        echo Error detected in input settings! Abort.
        exit 1
    fi

    if [[ ${do_setx} = true ]]
    then
        set -x
    fi

    echo "=== Input Settings ==="
    echo "Input args: ${ARGS[@]}"
    for key in "${!vars[@]}"
    do
        echo "--${key,,}=""${vars[${key}]}"
    done
}
