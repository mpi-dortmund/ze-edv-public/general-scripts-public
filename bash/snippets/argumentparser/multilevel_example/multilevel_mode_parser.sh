# This script is a multi level mode parser and works on a file system level
# FOLDER
# - script.sh
# - MODULES
# - MODULES/mode1 <-translates to mode1
# - MODULES/mode1/submode1.sh <- bash script
# - MODULES/mode2 <- translates to mode2
# - MODULES/mode2/submode1.sh <- bash script
# - MODULES/mode2/old/ <- translates to mode2.old
# - MODULES/mode2/old/submode1.sh <- bash script

function get_base_dir(){
    start_dir=$(realpath $(dirname "${1}")) # 1 is the script file
    while true
    do
        if [[ -f ${start_dir}/.BASE_DIR_INDICATOR ]]
        then
            break
        elif [[ $(realpath ${start_dir}) = '/' ]]
        then
            echo Root directory reached but indicator file .BASE_DIR_INDICATOR not found! 1>&2
            return 1
        else
            start_dir=${start_dir}/..
        fi
    done
    echo $(realpath ${start_dir})
}

BASE_SCRIPT="${0}" # PATH of this script
SELECTED_MODE="${1}" # Selected First level mode
SELECTED_SUBMODE="${2}" # Selected Second level mode
# OPTION arguments are stored in "${@:3}" and need to be directly provided at the bottom
####

function get_module(){
    local current_mode="${1}" # MODE or MODE_NAME
    local aim_mode="${2}" # MODE or SUBMODE
    local mode_directory="${3}" # Directory of the current_mode
    local user_selected_mode="${4}" # Directory of the current_mode

    # Create help dynamically on the input
    local usage="bash $(realpath ${BASE_SCRIPT}) ${current_mode} SUBMODE OPTIONS
    Possible values for ${aim_mode} are:

      help - Show this help.
      ---
    "

    local mode_name="help" # Default mode
    local mode_path="" # Default path is empty

    # Search for files depending on the aim_mode
    if [[ ${aim_mode} = "SUBMODE" ]]
    then
        # Search all .sh files in the provided directory
        # Search only in this directory and no subdirectories
        # Search only files
        # Needs to end with .sh
        # Cannot be hidden
        # Ignore help.sh
        local find_paths=$(
            find ${mode_directory} \
                -maxdepth 1 \
                -type f \
                -path '*.sh' \
                -not -path '*/\.*' \
                -not -name '*help.sh' \
                -not -name '*000*.sh' \
            | sort
        )
        local help_file='' # File containing the info (in this mode the info is in the found file itself)
    elif [[ ${aim_mode} = "MODE" ]]
    then
        # Search all directories and subdirectories
        # Ignore the starting directory itself
        local find_paths=$(
            find ${mode_directory} \
                -type d \
                -not -path "${mode_directory}" \
            | sort
        )
        local help_file='/help.sh' # File containing the info (in this mode the info is in the help.sh file
    else
        # Sanity check for the programmer and return 1 if not true
        echo "Unreachable code: ${search_type} not known!"
        return 1
    fi

    local path
    for path in ${find_paths}
    do
        # Remove the starting directory
        # Replace / with . symbols (MODE option)
        # Remove .sh file ending (SUBMODE option)
        local module_name=$(
            echo ${path} \
            | sed "s|${mode_directory}/||g" \
            | sed "s|/|.|g" \
            | sed "s|\.sh$||g"
        )
        local module_info=$(
            source ${path}${help_file} &>/dev/null \
                && module_info 2>/dev/null
        ) # Run the module_info function of the helpfile

        if [[ -z ${module_info} ]]
        then
            # If no help is provided put some default text
            module_info="${path}"
        fi

        # Add to usage
        usage+="  ${module_name,,} - ${module_info}
    "
        if [[ ${module_name,,} = ${user_selected_mode,,} ]]
        then
            # If the selected module is found, set the mode_path and mode_name
            mode_name=${module_name,,}
            mode_path=${path}
        fi
    done

    if [[ ${mode_name} = "help" ]]
    then
        # Echo the help to standard err and return 1
        echo "${usage}" 1>&2 
        return 1
    fi

    # Sanity check for the programmer and return 1 if not true
    test -z ${mode_path} && echo "Unreachable code in ${current_mode}: mode_path is empty." 1>&2 \
        && return 1

    # Return the path of the mode
    echo ${mode_path}

}

SCRIPT_BASE_DIR=$(get_base_dir "${BASE_SCRIPT}") || exit 1 # Directory of this script
# Setting the environmental variables like MODULE_DIR
source ${SCRIPT_BASE_DIR}/shared_functions/get_env.sh

# Check the selected mode (exit if non provided or unkown)
submodule_dir=$(get_module MODE MODE ${MODULE_DIR} ${SELECTED_MODE}) \
    || exit 1
# Check the selected submode (exit if non provided or unkown)
submodule_file=$(get_module ${SELECTED_MODE} SUBMODE ${submodule_dir} ${SELECTED_SUBMODE}) \
    || exit 1

# Run the "run" function of the identified submodule file with all provided options
bash ${submodule_file} "${@:3}" || exit 1

echo "All done"
