#!/bin/bash
ENTRY_HERE=$(realpath $(dirname ${0}))

function get_base_dir(){
	start_dir=$(realpath $(dirname "${1}")) # 1 is the script file
	while true
	do
		if [[ -f ${start_dir}/.BASE_DIR_INDICATOR ]]
		then
			break
		elif [[ $(realpath ${start_dir}) = '/' ]]
		then
			echo Root directory reached but indicator file .BASE_DIR_INDICATOR not found! 1>&2
			return 1
		else
			start_dir=${start_dir}/..
		fi
	done
	echo $(realpath ${start_dir})
}

SCRIPT_BASE_DIR=$(get_base_dir "${0}") || exit 1 # Directory of this script
source ${SCRIPT_BASE_DIR}/shared_functions/argumentparser_dynamic.sh

function run_entry(){
	local submodule=${1}

	declare -A original_vars
	declare -A vars
	source $(dirname ${submodule})/../000_shared_commandline.sh
	source $(dirname ${submodule})/000_commandline.sh
	original_vars=()
	for key in "${!global_vars[@]}"
	do
		original_vars[${key}]="${global_vars[${key}]}"
	done
	for key in "${!module_vars[@]}"
	do
		original_vars[${key}]="${module_vars[${key}]}"
	done
	dynamic_parser "${@:2}"

	source ${SCRIPT_BASE_DIR}/shared_functions/get_env.sh

	source ${submodule}
	run
}

if ! (return 0 2>/dev/null)
then
	run_entry "${@}"
fi
