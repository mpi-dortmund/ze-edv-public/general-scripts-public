# This file contains the possible command line arguments of this module.
# Not every submode requires every option, but it does not harm to provide more than necessary

# The resulting content can be accessed via: ${vars['KEY']}
# Defined variables will overwrite values in ../000_shared_commandline.sh

declare -A module_vars
module_vars=(
  ###
  #
  # Custom arguments
  #
  ###
  ["module_arg1"]="module_arg1 ${OPTION_SEP} MODULE ARGUMENT 1 ${OPTION_SEP} ANY"
  ["module_arg2"]="module_arg2 ${OPTION_SEP} MODULE ARGUMENT 2 ${OPTION_SEP} ANY"

  ###
  #
  # Default Args
  #
  ###
)
