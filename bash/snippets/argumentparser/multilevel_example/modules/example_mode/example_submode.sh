#!/bin/bash
HERE=$(realpath $(dirname ${0}))
MODULE_INFO="Example help description of this submodule."

function run(){
    ### PUT HERE THE CODE THAT YOU WANT TO USE
    ### COMMAND LINE ARGUMENTS CAN BE ACCESSED VIA ${vars["COMMAND"]}
    ### POSSBILE COMMANDS ARE DEFINED IN THE 000_entry.sh
    echo "I AM AN EXAMPLE SUBMODULE"
}


# Execute the run function if explicitely executed and not sourced
if ! (return 0 2>/dev/null)
then
	source ${HERE}/../000_entry.sh && run_entry "${0}" "${@}"
fi
