#!/bin/bash
HERE=$(realpath $(dirname ${0}))
MODULE_INFO="Example help description of this mode."

function get_base_dir(){
    start_dir=$(realpath $(dirname "${1}")) # 1 is the script file
    while true
    do
        if [[ -f ${start_dir}/.BASE_DIR_INDICATOR ]]
        then
            break
        elif [[ $(realpath ${start_dir}) = '/' ]]
        then
            echo Root directory reached but indicator file .BASE_DIR_INDICATOR not found! 1>&2
            return 1
        else
            start_dir=${start_dir}/..
        fi
    done
    echo $(realpath ${start_dir})
}

if ! (return 0 2>/dev/null)
then
    SCRIPT_BASE_DIR=$(get_base_dir "${0}") || exit 1 # Directory of this script
fi
source ${SCRIPT_BASE_DIR}/shared_functions/get_env.sh
