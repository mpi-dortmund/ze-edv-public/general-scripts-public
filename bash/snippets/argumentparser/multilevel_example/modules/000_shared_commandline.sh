# Do not call this file directly!
# This file should be sourced by the 000_commandline.sh file in the module directories.
# The content of this values can be locally overwritten by re-assigning them there.

declare -A global_vars
global_vars=(
  ###
  #
  # Custom arguments
  #
  ###
  ["SHARED_ARG1"]="SHARED_ARG1 ${OPTION_SEP} SHARED ARGUMENT 1 ${OPTION_SEP} ANY"
  ["SHARED_ARG2"]="SHARED_ARG2 ${OPTION_SEP} SHARED ARGUMENT 2 ${OPTION_SEP} ANY"

  ###
  #
  # Default Args
  #
  ###
  ["VERBOSE_SETX_SUPPRESS"]="false ${OPTION_SEP} Enable set -x after option parsing ${OPTION_SEP} BOOL"
  ["HELP_SUPPRESS"]="false ${OPTION_SEP} Show the usage instructions. ${OPTION_SEP} BOOL"
  ["ARGUMENTS"]="options to ignore ${OPTION_SEP} Ignore options ${OPTION_SEP} ANY"
)

