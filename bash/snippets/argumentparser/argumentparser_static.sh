# Static argumentparser example

function func1(){
    echo "I am func1"
}

function func2(){
    echo "I am func2"
}

function funcN(){
    echo "I am funcN"
}

function help(){
    # Needs to exit
    echo "I am help"
    exit 1
}


FONT_DEFAULT="$(tput sgr0)"
FONT_RED="$(tput setaf 1)"
ERROR="${FONT_RED}ERROR:${FONT_DEFAULT}"
# If no arguments are provided raise help
if test $# -eq 0; then help; fi
# Loop through all command line arguments
while [ $# -gt 0 ]; do
case "$1" in
  -h|--help) help;;
  -f1|--func1) shift; func1;;
  -f2|--func2) shift; func2;;
  -fN|--funcN) shift; funcN;;
  -t|--test) shift; echo TEST;;
  # This means anythin else undefined
  *) echo "${ERROR} invalid option $1".; help;;
esac
done
