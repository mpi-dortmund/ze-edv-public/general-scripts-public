# This script shows a very simple multithreading approach for tasks that are not
# Computational haevy and in general short.
# This method can lead to a huge overhead for tasks with varying run times

tasks=(1 2 3 4 5 6)
MAX_THREADS=4

function do_task(){
    # $1 - Task to do
    echo I am task $1
    sleep 1
}

for task_id in $(seq 0 ${MAX_THREADS} $(( ${#tasks[@]} - 1)) )
do
    for thread_id in $(seq 0 $(( ${MAX_THREADS} - 1 )) )
    do
        array_index=$(( ${task_id} + ${thread_id} ))
        if [[ ${array_index} -lt ${#tasks[@]} ]]
        then
            do_task ${tasks[${array_index}]} &
        fi
    done
    wait 
done

echo "All done"
