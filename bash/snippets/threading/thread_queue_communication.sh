# A file based multithreading approach allowing to process multiple problems at once
# This is a very efficient method for larger tasks that might vary in execution time.

# Number of tasks to calculate in parallel
MAX_THREADS=4

# Create a thread_file_folder with a unique timestamp plus random number
THREAD_FILES_FOLDER=THREADS_$(date +%s)_${RANDOM}
mkdir -p ${THREAD_FILES_FOLDER}
# Set a prefix for the thread files
THREAD_FILE_PREFIX=${THREAD_FILES_FOLDER}/THREAD_
TASK_FILE_PREFIX=${THREAD_FILES_FOLDER}/TASK_

# The arguments for the tasks to be processed are best stored in an array
tasks_args=(1 2 3 4 5 6 7 8 9 10 11 12 13 14)

# Function to be run in parallel
function do_task(){
    # $1 - Allocated thread file
    # $2 - Output file to store task results
    # ${@:3} - Task options
    result=$((${3} * 100))
    echo I am the task number ${@:2} to be processed in parallel with result: ${result}
    echo ${result} > ${2}
    sleep 1
    echo '' > ${1}
}

function combine_task(){
    # $1 - Task file prefix
    # In this example the average of the results is calculated
    tasks_files=($(ls $1*))
    sum=$(cat ${tasks_files[@]} | paste -sd+ | bc)
    echo Average:
    echo "${sum} / ${#tasks_files[@]}" | bc -l
}

function check_ready(){
    # $1 - Max number of threads
    # $2 - Thread file prefix
    # $3 - Task specifier
    allocated_thread_file=false
    while [[ ${allocated_thread_file} = false ]]
    do
        for thread_id in $(seq 1 ${1})
        do
            thread_file=${2}${thread_id}
            if [[ -z ${thread_file} ]]
            then
                allocated_thread_file=${thread_file}
                echo ${3} > ${allocated_thread_file}
                break
            fi
        done
        sleep 0.1
    done
    echo Task ${3} started in thread $(basename ${allocated_thread_file})
}

# Create thread queue files
for thread_id in $(seq 1 ${MAX_THREADS})
do
    touch ${THREAD_FILE_PREFIX}${thread_id}
done

# Loop over all tasks
for task_id in $(seq 0 $(( ${#tasks_args[@]} - 1 )))
do
    allocated_thread_file=$(check_ready ${MAX_THREADS} ${THREAD_FILE_PREFIX} ${task_id})

    task_id_file=${TASK_FILE_PREFIX}${task_id}
    do_task ${allocated_thread_file} ${task_id_file} ${tasks_args[${task_id}]} &
done
# Wait for all threads to finish
wait

# Do something with the created multithreading output
combine_task ${TASK_FILE_PREFIX}

# Remove the thread files
rm -r ${THREAD_FILES_FOLDER}
echo 'All tasks done'
