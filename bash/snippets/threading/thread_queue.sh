# A file based multithreading approach allowing to process multiple problems at once
# This is a very efficient method for larger tasks that might vary in execution time.

# Number of tasks to calculate in parallel
MAX_THREADS=4

# Create a thread_file_folder with a unique timestamp plus random number
THREAD_FILE_FOLDER=THREADS_$(date +%s)_${RANDOM}
mkdir -p ${THREAD_FILE_FOLDER}
# Set a prefix for the thread files
THREAD_FILE_PREFIX=${THREAD_FILE_FOLDER}/THREAD_

# The arguments for the tasks to be processed are best stored in an array
tasks_args=(1 2 3 4 5 6 7 8 9 10 11 12 13 14)

# Function to be run in parallel
function do_task(){
    # $1 - Allocated thread file
    # ${@:2} - Task options
    echo I am the task number ${@:2} to be processed in parallel
    sleep 1
    echo '' > ${1}
}

function check_ready(){
    # $1 - Max number of threads
    # $2 - Thread file prefix
    # $3 - Task description
    allocated_thread_file=false
    while [[ ${allocated_thread_file} = false ]]
    do
        for thread_id in $(seq 1 ${1})
        do
            thread_file=${2}${thread_id}
            if [[ -z $(cat ${thread_file}) ]]
            then
                allocated_thread_file=${thread_file}
                echo ${3} > ${allocated_thread_file}
                break
            fi
        done
        sleep 0.1
    done
    echo Task ${3} started in thread $(basename ${allocated_thread_file})
}

# Create thread queue files
for thread_id in $(seq 1 ${MAX_THREADS})
do
    touch ${THREAD_FILE_PREFIX}${thread_id}
done

# Loop over all tasks
for task in ${tasks_args[@]}
do
    allocated_thread_file=$(check_ready ${MAX_THREADS} ${THREAD_FILE_PREFIX} ${task})

    do_task ${allocated_thread_file} ${task} &
done
# Wait for all threads to finish
wait

# Remove the thread files
rm -r ${THREAD_FILE_FOLDER}
echo 'All tasks done'
