# This script shows a very simple multithreading approach for tasks that are not
# Computational haevy and in general short.
# DO NOT USE THIS METHOD FOR A LARGE NUMBER OF TASKS.

tasks=(1 2 3 4 5 6)

function do_task(){
    # $1 - Task to do
    echo I am task $1
    sleep 1
}

for task in ${tasks[@]}
do
    do_task ${task} &
done
wait

echo "All done"
